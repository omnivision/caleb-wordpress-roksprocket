<?php
/**
 * @version   $Id$
 * @author    RocketTheme http://www.rockettheme.com
 * @copyright Copyright (C) 2007 - 2013 RocketTheme, LLC
 * @license   http://www.gnu.org/licenses/gpl-2.0.html GNU/GPLv2 only
 */

class RokSprocket_Provider_Caleb extends RokSprocket_Provider_AbstarctWordpressBasedProvider
{
	/**
	 * @static
	 * @return bool
	 */
    public static function isAvailable()
   	{
		return class_exists( 'WP_Widget' );
   	}

	/**
	 * @param array $filters
	 * @param array $sort_filters
	 */
	public function __construct($filters = array(), $sort_filters = array())
	{
		parent::__construct('caleb');
		$this->setFilterChoices($filters, $sort_filters);
	}

	public function getItems() {
		global $q_config;
		add_filter( 'wp_feed_cache_transient_lifetime', array( &$this, 'set_cache_time' ) );
		$bits = array('lang'=>'','types'=>'','categories'=>'','countries'=>'');

		/* If articles are requested, ask for it in the current language */
    		$langCode = $q_config['language'];
		$source = "http://www.om.org/rss/resources.rss?1=1";
		if (in_array(4,$this->params->get('caleb_resource_types')))
			$bits['lang']="&text=LANG_".$langCode;

		foreach ($this->params->get('caleb_resource_types') as $resource_type_id)
			$bits['types'] .= '&mediaTypeId='.$resource_type_id;

		if (is_array($this->params->get('caleb_categories')))
			foreach ($this->params->get('caleb_categories') as $category_id)
				$bits['categories'] .= "&categoryId=".$category_id;

		foreach ($this->params->get('caleb_countries') as $country_id)
		{
			$bits['countries'] .= "&countryId=".$country_id;
		}
		$source_exact = $source.$bits['lang'].$bits['types'].$bits['categories'].$bits['countries'];
		$feed = fetch_feed( $source_exact );
		$maxitems = $feed->get_item_quantity(0); // return all items 
		/* If no results are returned, remove the parameter that has been specified from the URL.
	 	 * hopefully that will bring some results. */
		if ($maxitems < 1) {
			switch ($this->params->get('caleb_fallback_skip')) {
				case "countries": $bits['countries'] = ''; break;
				case "categories": $bits['categories'] = ''; break;
				case "language": $bits['lang'] = ''; break;
			}
			$source_fallback = $source.$bits['lang'].$bits['types'].$bits['categories'].$bits['countries'];
			$feed = fetch_feed( $source_fallback );
		}
		remove_filter( 'wp_feed_cache_transient_lifetime', array( &$this, 'set_cache_time' ) );

		$collection = new RokSprocket_ItemCollection();
		if( ! is_wp_error( $feed ) ) {
			$maxitems = $feed->get_item_quantity(0); // return all items 
			$rss_items = $feed->get_items(0, $maxitems);

			foreach( $rss_items as $raw_item ) {
				$item = $this->convertRawToItem( $raw_item );
				$collection[$item->getId()] = $item;
			}
		}

		$this->mapPerItemData($collection);
		return $collection;
	}

	public function set_cache_time( $seconds ) {
		return $this->params->get( 'caleb_cache', 12 ) * 60 * 60;
	}

	protected function convertRawToItem($raw_item, $dborder = 0) {
		$item = new RokSprocket_Item();
		$item->setProvider('caleb');
		$item->setId( md5($raw_item->get_ID()) ); // md5 the ID to remove weird characters
		$item->setAlias($raw_item->get_title());
		$title = $raw_item->get_item_tags('http://app.om.org/dtd/rss.dtd','title');
		$item->setTitle($title[0]['data']);
		$item->setDate($raw_item->get_date());
		$item->setPublished(true);
		$text = $raw_item->get_item_tags('http://app.om.org/dtd/rss.dtd','description');
		$item->setText($text[0]['data']);
		$item->setAuthor($raw_item->get_author());
		$item->setCategory($raw_item->get_category());
		$item->setHits(null);
		$item->setRating(null);
		$item->setMetaKey(null);
		$item->setMetaDesc(null);
		$item->setMetaData(null);

        //set up images array
        $images = array();
        $image = new RokSprocket_Item_Image();
		$thumbnailUrl = $raw_item->get_item_tags('http://app.om.org/dtd/rss.dtd','thumbnailUrl');
        $source = (String)$thumbnailUrl[0]['data'];
		$source = str_replace("/t/",'/m/',$source) ; // Get medium sized image.
		if( $this->params->get( 'caleb_download_images', 0 ) ) {
			$source = $this->download_image_copy( $source );
		}
        if ($source)
        {
		    $image->setSource( $source );
            $image->setIdentifier('image_thumbnail');
            $image->setCaption( $this->get_image_caption( $images_matches[0][0] ) );
            $image->setAlttext( $this->get_image_caption( $images_matches[0][0] ) );
            $images[$image->getIdentifier()] = $image;
        
            $item->setImages($images);
            $item->setPrimaryImage($images['image_thumbnail']);
        }

		$primary_link = new RokSprocket_Item_Link();
		$permalink = $raw_item->get_permalink();
		if ($this->params->get('local_url_enable')) {
			$article_id = substr($permalink,27); 
			$permalink = get_bloginfo('url')."/".$this->params->get('caleb_details_page')."/?resourceID=".$article_id;
		}
		if ($this->params->get('caleb_details_page')=='image')
			$permalink = add_query_arg('rokbox','yes',$permalink);
		$primary_link->setUrl( $permalink );
		$primary_link->getIdentifier('article_link');
		$item->setPrimaryLink( $primary_link );

		return $item;
	}
	public function get_image_caption( $image_tag ) {
		preg_match( "/alt=\"(.+?)\"/", $image_tag, $matches );
		return isset( $matches[1] ) ? $matches[1] : '';
	}

	function download_image_copy( $url ) {
		require_once(ABSPATH . 'wp-admin/includes/file.php');
		require_once(ABSPATH . 'wp-admin/includes/media.php');

		$_info = pathinfo( $url );
		$upload_dir = wp_upload_dir();
		$filename = 'm'.$_info['basename'];
        $filepath = $upload_dir['path'].DS.$filename;
        $fileurl = $upload_dir['url'].DS.$filename;
        if (file_exists($filepath)) return $fileurl;

		$tmp = download_url( $url );
		if( is_wp_error( $tmp ) ) {
			@unlink( $tmp );
			return $url;
		}
		$pathparts = pathinfo( $tmp );

		// fix file extension
		$result = copy($tmp, $filepath );
		@unlink( $file_array['tmp_name'] );

		return ( $result ) ? trailingslashit( $upload_dir['url'] ) . $filename : $url;
	}

    /**
     * @param $id
     *
     * @return string
     */
    protected function getArticleEditUrl($id)
    {
        return;
    }

    /**
     * @return array the array of image type and label
     */
    public static function getImageTypes()
    {
        return;
    }

    /**
     * @return array the array of link types and label
     */
    public static function getLinkTypes()
    {
        return;
    }

    /**
     * @return array the array of link types and label
     */
    public static function getTextTypes()
    {
        return;
    }
}
